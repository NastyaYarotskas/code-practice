package com.netcracker.edu.belkevich;

import java.math.BigDecimal;

public class OrderItem {

    private String product;

    private Integer quantity;

    private BigDecimal value;

    private OrderItem(String product, Integer quantity, BigDecimal value) {
        this.product = product;
        this.quantity = quantity;
        this.value = value;
    }

    public String getProduct() {
        return product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public BigDecimal getValue() {
        return value;
    }

    public static OrderItem of(String product, Integer quantity, BigDecimal value) {
        return new OrderItem(product, quantity, value);
    }
}
