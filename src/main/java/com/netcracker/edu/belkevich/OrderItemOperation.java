package com.netcracker.edu.belkevich;

import java.util.function.Function;

@FunctionalInterface
public interface OrderItemOperation extends Function<OrderItem, OrderItem> {
}
