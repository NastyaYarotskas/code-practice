package com.netcracker.edu.belkevich;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

public class Order {

    private Long id;

    private OrderNumber number;

    private OrderItems orderItems;

    private OrderItems discountedItems;


    private Order(OrderNumber number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public OrderNumber getNumber() {
        return number;
    }

    public OrderItems getOrderItems() {
        return OrderItems.of(orderItems);
    }

    public OrderItems getDiscountedItems() {
        return OrderItems.of(discountedItems);
    }

    public void addItem(OrderItem item) {
        if (!orderItems.contains(item)) {
            orderItems.add(item);
        }
    }

    public void apply(OrderItemOperation operation) {
        orderItems = OrderItems.of(orderItems
                .stream()
                .map(operation)
                .collect(Collectors.toList()));
    }

    public static Order of(OrderNumber number) {
        checkNotNull(number);
        return new Order(number);
    }
}
