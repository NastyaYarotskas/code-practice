package com.netcracker.edu.belkevich.entity;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UsersInformations implements Information {

    @Override
    public List<User> getUsersFilteredByPredicate(Predicate<User> predicate,List<User> users) {
        return users.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
