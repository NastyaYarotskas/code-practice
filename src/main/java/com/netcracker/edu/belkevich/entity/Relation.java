package com.netcracker.edu.belkevich.entity;

public enum Relation {

    FRIEND,
    RELATIVE,
    COLLEAGUE
}
