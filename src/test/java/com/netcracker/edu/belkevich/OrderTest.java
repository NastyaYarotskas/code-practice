package com.netcracker.edu.belkevich;


import com.netcracker.edu.belkevich.entity.Information;
import com.netcracker.edu.belkevich.entity.User;
import com.netcracker.edu.belkevich.entity.UsersInformations;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class OrderTest {

    private Information informations = new UsersInformations();

    @Test
    public void Test(){
        OrderItem item = OrderItem.of("book", 5, BigDecimal.valueOf(10));
        Order order = Order.of(OrderNumber.of(1));
        order.addItem(item);
        order.getOrderItems().getQuantity();
        order.getDiscountedItems().getQuantity();
        order.apply(OrderItemOperations.DISCOUNT);

        List<User> users = new ArrayList<>();

        Predicate<User> agePredicate = user -> user.getAge() == 5;
        Predicate<User> namePredicate = user -> user.getName().equals("Alexey");

        List<User> usersWithAgeFive = informations.getUsersFilteredByPredicate(agePredicate, users);
        List<User> usersWithNameAlexey = informations.getUsersFilteredByPredicate(namePredicate, users);
    }

}
